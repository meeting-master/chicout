## Node image
FROM golang:1.16-buster AS build

## Env variables
ENV TINI_VERSION v0.19.0


# Add Tini
# https://github.com/krallin/tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# Create app directory
ENV APP_HOME /go/src/app
WORKDIR $APP_HOME

# Bundle app source
COPY . .
# Install dependancie
RUN go mod download
RUN go mod verify
#build process
RUN CGO_ENABLED=0 go build -o es-api

# deploy
FROM alpine

ENV APP_HOME /go/src/app
WORKDIR $APP_HOME
COPY --from=build  $APP_HOME/es-api $APP_HOME

EXPOSE 8080
CMD ["./es-api"]
