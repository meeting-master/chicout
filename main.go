package main

import (
	"fmt"
	"io"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/meeting-master/chicout/handler"
	"gitlab.com/meeting-master/sdk/connector/pgsql"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	envFile := ".env"
	if s, ok := os.LookupEnv("ENV_FILE"); ok {
		envFile = s
	}
	if err := godotenv.Load(envFile); err != nil {
		panic(err)
	}
	pg, err := pgsql.Open()
	if err != nil {
		panic(err)
	}
	defer pg.Close()

	lpg, err := pgsql.OpenWithConfig(pgsql.Config{
		HostName:          os.Getenv("POSTGRES_HOSTNAME"),
		UserName:          os.Getenv("POSTGRES_USERNAME"),
		Password:          os.Getenv("POSTGRES_PASSWORD"),
		Port:              5432,
		DataBaseName:      os.Getenv("LOCATION_DB"),
		MaxIdleConnection: 3,
		MaxOpenConnection: 5,
	})

	if err != nil {
		panic(err)
	}

	defer lpg.Close()
	f, err := os.OpenFile(fmt.Sprintf("%s%s%s", os.Getenv("LOG_DIR"), time.Now().UTC().Format("20060102"), ".log"),
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(fmt.Sprintf("Open Log File Failed %v", err))
	}
	defer f.Close()
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
	router := gin.Default()
	router.Use(corsConfig())
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC3339),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))
	router.Use(gin.Recovery())
	router = handler.Setup(router, pg, lpg)
	if err := router.Run(":8080"); err != nil {
		panic(err)
	}
}

func corsConfig() gin.HandlerFunc {
	config := cors.Config{
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		MaxAge:           12 * time.Hour,
	}
	return cors.New(config)
}
