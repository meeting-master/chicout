package handler

import (
	"context"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	messagingRepo "gitlab.com/meeting-master/sdk/messaging/repository"
	"log"
	"net/http"
	"strings"
	"time"

	authRepo "gitlab.com/meeting-master/sdk/auth/repository"
	authorization "gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/calendar/repository"
	"gitlab.com/meeting-master/sdk/calendar/service"
	"gitlab.com/meeting-master/sdk/connector/pgsql"

	"github.com/gin-gonic/gin"
)

//schedule is refer to every operation for configuration
//like set timetable or update it
//schedule is set for an office binding to a service
//schedule operation is to set working days and hours
//
//Calendar is refer to a hole view off appointments scheduled

type CalendarHandler struct {
	service service.CalendarService
}

func newCalendarHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewCalendarRepository(db)
	auth := authorization.NewAuthService(authRepo.NewAuthRepository(db))
	mr := messagingRepo.NewMessagingRepository(db)
	eng := engine.Init(mr)
	s := service.InitWithEngine(repo, auth, eng)

	handler := &CalendarHandler{service: s}

	calendarGroup := router.Group("/calendar/")
	{
		calendarGroup.POST("/schedule", handler.SetSchedule)
		calendarGroup.GET("/schedule", handler.GetSchedule)
		calendarGroup.PUT("/schedule", handler.UpdateSchedule)
		calendarGroup.POST("/calendar", handler.GetCalendar)

		calendarGroup.POST("/generate-time-slots", handler.CreateTimeSlots)
		calendarGroup.GET("/calendar-history", handler.CalendarCreationHistory)
		calendarGroup.GET("/time-slot", handler.TimeSlot)
		calendarGroup.GET("/time-slot-by-dates", handler.TimeSlotsByDates)
		calendarGroup.GET("/time-slot-by-date-range", handler.TimeSlotsByDateRange)
		calendarGroup.PUT("/time-slot", handler.UpdateTimeSlot)
		calendarGroup.GET("/time-slot-appointments", handler.TimeSlotAppointments)

		calendarGroup.POST("/appointment", handler.CreateAppointment)
		calendarGroup.PUT("/appointment", handler.ConsumeAppointment)
		calendarGroup.GET("/appointment", handler.Appointment)
		calendarGroup.DELETE("/appointment", handler.DeleteAppointment)
		calendarGroup.GET("/appointment-history", handler.AppointmentHistory)
		calendarGroup.POST("/rating", handler.InsertRating)
		calendarGroup.POST("/device-token", handler.DeviceToken)
	}

	return router
}

func (h *CalendarHandler) SetSchedule(gc *gin.Context) {
	var request model.ScheduleRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	err := h.service.InsertSchedule(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, nil)
}

func (h *CalendarHandler) GetSchedule(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		log.Println(er)
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	officeID := gc.Query("officeId")
	serviceID := gc.Query("serviceId")

	res, err := h.service.Schedule(ctx, officeID, serviceID)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *CalendarHandler) TimeSlotsByDates(gc *gin.Context) {
	officeID := gc.Query("officeId")
	serviceID := gc.Query("serviceId")
	values := gc.QueryArray("dates[]")
	dates := make([]time.Time, len(values))
	for k, v := range values {
		d, _ := time.Parse(time.RFC3339, v)
		dates[k] = d
	}
	res, err := h.service.TimeSlotsByDates(officeID, serviceID, dates...)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *CalendarHandler) TimeSlotsByDateRange(gc *gin.Context) {
	/*_, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		log.Println(er)
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}*/

	officeID := gc.Query("officeId")
	serviceID := gc.Query("serviceId")
	sd := gc.Query("startDate")
	ed := gc.Query("endDate")
	startDate, _ := time.Parse(time.RFC3339, sd)
	endDate, _ := time.Parse(time.RFC3339, ed)

	res, err := h.service.TimeSlotsByDateRange(officeID, serviceID, startDate, endDate)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}


func (h *CalendarHandler) CalendarCreationHistory(gc *gin.Context) {
	_, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		log.Println(er)
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	officeID := gc.Query("officeId")
	serviceID := gc.Query("serviceId")

	res, err := h.service.CalendarCreationHistory(officeID, serviceID)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *CalendarHandler) UpdateSchedule(gc *gin.Context) {
	var request model.ScheduleUpdate
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	schedules, err := h.service.UpdateSchedule(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: schedules,
	})
}

func (h *CalendarHandler) GetCalendar(gc *gin.Context) {
	var request model.CalendarRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	res, err := h.service.Calendar(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{Data: res})
}

func (h *CalendarHandler) CreateTimeSlots(gc *gin.Context) {
	var request model.TimeSlotRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	err := h.service.CreateTimeSlot(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, nil)
}

func (h *CalendarHandler) TimeSlot(c *gin.Context) {
	c.JSON(http.StatusOK, nil)
}

func (h *CalendarHandler) UpdateTimeSlot(c *gin.Context) {
	c.JSON(http.StatusOK, nil)
}

func (h *CalendarHandler) CreateAppointment(gc *gin.Context) {
	var request model.AppointmentRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		ctx = context.Background()
	}

	id, err := h.service.CreateAppointment(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{ID: id})
}

func (h *CalendarHandler) ConsumeAppointment(gc *gin.Context) {
	var request Request
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	err := h.service.ConsumeAppointment(ctx, request.ID)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, "")
}

func (h *CalendarHandler) Appointment(c *gin.Context) {
	c.JSON(http.StatusOK, nil)
}

func (h *CalendarHandler) TimeSlotAppointments(gc *gin.Context) {
	_, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	ids := gc.QueryArray("timeSlotIds[]")
	res, err := h.service.TimeSlotAppointments(ids...)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *CalendarHandler) DeleteAppointment(gc *gin.Context) {
	data := gc.Query("data")
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		ctx = context.Background()
	}
	split := strings.Split(data, "|")
	if len(split) != 2 {
		gc.JSON(http.StatusBadRequest, errors.InvalidRequestData())
	}
	err := h.service.CancelAppointment(ctx, split[0], split[1])
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, nil)
}
