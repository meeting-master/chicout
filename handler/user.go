package handler

import (
	"context"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	messagingRepo "gitlab.com/meeting-master/sdk/messaging/repository"
	"log"
	"net/http"

	authorizationRepo "gitlab.com/meeting-master/sdk/auth/repository"
	authorization "gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/user/model"
	"gitlab.com/meeting-master/sdk/user/repository"
	"gitlab.com/meeting-master/sdk/user/service"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	service service.UserService
}

func newUserHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewUserRepository(db)
	authRepo := authorizationRepo.NewAuthRepository(db)
	auth := authorization.NewAuthService(authRepo)
	mr := messagingRepo.NewMessagingRepository(db)
	eng := engine.Init(mr)
	s := service.InitWithEngine(repo, auth, eng)

	handler := &UserHandler{service: s}
	userGroup := router.Group("/user")
	{
		userGroup.POST("/activate", handler.Activate)
		userGroup.POST("/change-password", handler.ChangePassword)
		userGroup.POST("/create", handler.CreateUser)
		userGroup.POST("/login", handler.Login)
		userGroup.POST("/admin-login", handler.AdminLogin)
		userGroup.GET("/users", handler.Users)
		userGroup.GET("", handler.User)
		userGroup.PUT("", handler.UpdateInfo)
		userGroup.POST("/grant", handler.GrantUser)
		userGroup.GET("/privileges", handler.Privileges)
		userGroup.POST("/forgot-password", handler.ForgotPassword)
		userGroup.POST("/end-user", handler.UpsertEndUser)
		userGroup.GET("/end-user", handler.GetEndUser)
	}

	return router
}

func (h *UserHandler) Activate(gc *gin.Context) {
	var request Request
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	ctx = context.WithValue(ctx, appContext.ActivationCodeKey, request.ActivationCode)

	if err := h.service.Active(ctx); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, "")
}

func (h *UserHandler) ChangePassword(gc *gin.Context) {
	var request model.LoginRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	if err := h.service.ChangePassword(ctx, request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, "")
}

func (h *UserHandler) ForgotPassword(gc *gin.Context) {
	var request model.LoginRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	if er := h.service.ForgotPassword(request.UserName); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": er})
		return
	}
	gc.JSON(http.StatusOK, "")
}

func (h *UserHandler) CreateUser(gc *gin.Context) {
	var request model.UserCreateRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	id, code, err := h.service.Create(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	WriteCode(request.UserName, code)
	gc.JSON(http.StatusOK, Response{ID: id})
}

func (h *UserHandler) UpdateInfo(gc *gin.Context) {
	var request model.UserUpdateRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	err := h.service.UpdateInfo(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, "")
}


func (h *UserHandler) GrantUser(gc *gin.Context) {
	var request model.GrantRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, err := GenerateContext(gc.GetHeader("Authorization"))
	if err != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	if err := h.service.Grant(ctx, request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{})
}

func (h *UserHandler) Login(c *gin.Context) {
	var request model.LoginRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	connection, err := h.service.Login(request)
	if err != nil {
		log.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, Response{Data: connection})
}

func (h *UserHandler) AdminLogin(gc *gin.Context) {
	var request model.LoginRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	connection, err := h.service.Login(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{Data: connection})
}

func (h *UserHandler) Users(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	res, err := h.service.Users(ctx)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *UserHandler) User(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	res, err := h.service.User(ctx)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *UserHandler) Privileges(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	userID := gc.Query("userId")
	res, err := h.service.Privileges(ctx, userID)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}
