package handler

import (
	"encoding/json"
	"gitlab.com/meeting-master/chicout/handler/image"
	authRepo "gitlab.com/meeting-master/sdk/auth/repository"
	authorization "gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	messagingRepo "gitlab.com/meeting-master/sdk/messaging/repository"
	"log"
	"net/http"
	"strings"

	"gitlab.com/meeting-master/sdk/announce/model"
	"gitlab.com/meeting-master/sdk/announce/repository"
	"gitlab.com/meeting-master/sdk/announce/service"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"

	"github.com/gin-gonic/gin"
)

type AnnounceHandler struct {
	service service.AnnounceService
}

func newAnnounceHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewAnnounceRepository(db)
	auth := authorization.NewAuthService(authRepo.NewAuthRepository(db))
	mr := messagingRepo.NewMessagingRepository(db)
	eng := engine.Init(mr)
	s := service.InitWithEngine(repo, auth, eng)

	handler := &AnnounceHandler{service: s}

	router.POST("/announce", handler.CreateAnnounce)
	router.PUT("/announce", handler.ModifyAnnounce)
	router.GET("/announce", handler.GetAnnounce)
	router.DELETE("/announce", handler.DeleteAnnounce)

	router.GET("/last-announces", handler.LastAnnounces)
	router.GET("/announces-by-tag", handler.AnnouncesByTag)
	router.GET("/announces-by-owner", handler.AnnouncesByOwner)
	router.GET("/announces-to-validate", handler.AnnouncesToValidate)
	router.POST("/announce-validate", handler.Validate)

	return router
}

func (h *AnnounceHandler) CreateAnnounce(gc *gin.Context) {
	var form AnnounceForm
	if er := gc.ShouldBind(&form); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	var request model.AnnounceRequest
	if er := json.Unmarshal([]byte(form.Data), &request); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	if form.Images != nil {
		urls, er := image.Upload(gc, image.TypeAnnounce, form.Images...)
		if er != nil {
			log.Println(er.Error())
			gc.JSON(http.StatusInternalServerError, "")
			return
		}
		 images := append(request.Pictures, urls...)
		request.Pictures = images
	}

	id, err := h.service.Upsert(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{ID: id})
}

func (h *AnnounceHandler) ModifyAnnounce(gc *gin.Context) {
	h.CreateAnnounce(gc)
}

func (h *AnnounceHandler) GetAnnounce(gc *gin.Context) {
	id := gc.Query("id")
	announce, err := h.service.AnnounceById(id)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{Data: announce})
}

func (h *AnnounceHandler) DeleteAnnounce(gc *gin.Context) {
	data := gc.Query("data")
	split := strings.Split(data, "|")
	if len(split) != 2 {
		gc.JSON(http.StatusBadRequest, errors.InvalidRequestData())
	}
	err := h.service.Delete(split[0], split[1])
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, nil)
}

func (h *AnnounceHandler) LastAnnounces(gc *gin.Context) {
	res, err := h.service.LastAnnounces()
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{Data: res})
}

func (h *AnnounceHandler) AnnouncesByTag(gc *gin.Context) {
	tag := gc.Query("tag")
	townId := gc.Query("townId")

	res, err := h.service.AnnouncesByTag(tag, townId)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *AnnounceHandler) AnnouncesByOwner(gc *gin.Context) {
	id := gc.Query("ownerId")
	res, err := h.service.AnnouncesByOwner(id)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *AnnounceHandler) AnnouncesToValidate(gc *gin.Context) {
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	res, err := h.service.AnnouncesToValidate(ctx)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}

func (h *AnnounceHandler) Validate(gc *gin.Context) {
	var request model.ValidationRequest
	if er := gc.ShouldBindJSON(&request); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	 err := h.service.Validate(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, "")
}
