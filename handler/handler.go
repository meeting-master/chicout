package handler

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/meeting-master/sdk/connector/pgsql"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/jwt"

	"github.com/gin-gonic/gin"
)

func Setup(router *gin.Engine, pg, lpg *pgsql.DB) *gin.Engine {
	router = newUserHandler(router, pg)
	router = newCalendarHandler(router, pg)
	router = newCustomerHandler(router, pg)
	router = newLocationHandler(router, pg)
	router = newAnnounceHandler(router, pg)
	return router
}

func GenerateContext(token string) (ctx context.Context, err error) {
	ctx = context.Background()
	values, err := jwt.ParseToken(strings.TrimPrefix(token, "Bearer "))
	if err != nil {
		log.Println(err)
		return
	}

	ctx = context.WithValue(ctx, appContext.ResourceIdKey, values.ResourceId)
	ctx = context.WithValue(ctx, appContext.UserIdKey, values.UserId)
	ctx = context.WithValue(ctx, appContext.UserNameKey, values.UserName)
	ctx = context.WithValue(ctx, appContext.AdminUserKey, values.IsAdmin)
	return
}

func WriteCode(username, code string) {
	f, _ := os.OpenFile("code.log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer f.Close()
	_, _ = f.WriteString(fmt.Sprintf("%v-%v\n", username, code))
	f.Sync()
}
