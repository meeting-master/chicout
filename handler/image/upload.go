package image

import (
	"github.com/gin-gonic/gin"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	TypeLogo = "logo"
	TypeAnnounce = "Ad"
	)

func Upload(gc *gin.Context, imageType string, images ...*multipart.FileHeader) ([]string, error) {
	var (
		logoBase = os.Getenv("LOGO_DESTINATION")
		announceBase = os.Getenv("ANNOUNCE_IMAGE_DESTINATION")
		logoUrl = os.Getenv("LOGO_URL")
		announceUrl = os.Getenv("ANNOUNCE_IMAGE_URL")
	)
	urls := make([]string, len(images))
	base := logoBase
	url := logoUrl
	if imageType != TypeLogo {
		base = announceBase
		url = announceUrl
	}
	for i, image := range images {
		img := strings.Split(image.Filename, ".")
		name := strconv.FormatInt(time.Now().UnixNano(), 10) + "." +  img[len(img)-1]
		destination := base + name
		if err := gc.SaveUploadedFile(image, destination); err != nil {
			return nil, err
		}
		urls[i] = url+name
	}

	return urls, nil
}
