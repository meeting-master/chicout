package handler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/meeting-master/chicout/handler/image"
	authorizationRepo "gitlab.com/meeting-master/sdk/auth/repository"
	authorization "gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	messagingRepo "gitlab.com/meeting-master/sdk/messaging/repository"
	"log"
	"net/http"
	"regexp"

	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/customer/service"
	"gitlab.com/meeting-master/sdk/errors"

	"github.com/gin-gonic/gin"
)

var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

type CustomerHandler struct {
	service service.CustomerService
}

func newCustomerHandler(router *gin.Engine, db *pgsql.DB) *gin.Engine {
	repo := repository.NewCustomerRepository(db)
	authRepo := authorizationRepo.NewAuthRepository(db)
	auth := authorization.NewAuthService(authRepo)
	mr := messagingRepo.NewMessagingRepository(db)
	eng := engine.Init(mr)
	s := service.InitWithEngine(repo, auth, eng)

	handler := &CustomerHandler{service: s}

	customerGroup := router.Group("/customer")
	{
		customerGroup.POST("/create", handler.CreateCustomer)
		customerGroup.PUT("/update", handler.UpdateCustomer)
		customerGroup.POST("/office", handler.CreateOffice)
		customerGroup.POST("/service", handler.CreateService)
		customerGroup.GET("/", handler.CustomerById)
		customerGroup.GET("/offices", handler.OfficesList)
		customerGroup.GET("/services", handler.ServicesList)
		customerGroup.GET("/office-services", handler.OfficeServicesList)
		customerGroup.GET("/office-by-tag", handler.OfficesByTag)
		customerGroup.GET("/statistics", handler.Statistics)
		customerGroup.GET("/office-rating", handler.OfficeRating)
		customerGroup.GET("/logos", handler.Logos)
		customerGroup.GET("/orders", handler.CustomerOrders)
		customerGroup.POST("/create-order", handler.CreateOrder)
	}

	return router
}

func (h *CustomerHandler) CreateCustomer(gc *gin.Context) {
	var request model.CustomerRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	if !rxEmail.MatchString(request.UserName) {
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.DBError(fmt.Errorf("invalid email for username"))})
		return
	}

	id, code, err := h.service.UpsertCustomer(request)

	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	WriteCode(request.UserName, code)
	gc.JSON(http.StatusOK, Response{
		ID:   id,
		Data: nil,
	})
}

func (h *CustomerHandler) UpdateCustomer(gc *gin.Context) {
	_, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	var form CustomerForm
	if er := gc.ShouldBind(&form); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	var request model.CustomerRequest

	if er := json.Unmarshal([]byte(form.Data), &request); er != nil {
		log.Println(er.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	if form.Logo != nil {
		urls, er := image.Upload(gc, image.TypeLogo, form.Logo)
		if er != nil {
			log.Println(er.Error())
			gc.JSON(http.StatusInternalServerError, "")
			return
		}
		request.LogoUrl = urls[0]
	}

	id, _, err := h.service.UpsertCustomer(request)

	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		ID:   id,
		Data: nil,
	})
}
func (h *CustomerHandler) CreateOffice(gc *gin.Context) {
	var request model.OfficeRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	id, err := h.service.UpsertOffice(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{ID: id})
}

func (h *CustomerHandler) CreateService(gc *gin.Context) {
	var request model.ServiceRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}
	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}

	id, err := h.service.UpsertService(ctx, request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{ID: id})
}

func (h *CustomerHandler) CustomerById(gc *gin.Context) {
	id := gc.Query("customerId")
	customer, err := h.service.CustomerById(id)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: customer,
	})
}

func (h *CustomerHandler) CustomerOrders(gc *gin.Context) {
	id := gc.Query("customerId")

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	orders, err := h.service.CustomerOrders(ctx, id)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: orders,
	})
}

func (h *CustomerHandler) CreateOrder(gc *gin.Context) {
	var request model.OrderRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	ctx, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	 err := h.service.CreateOrder(ctx, request)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, "")
}

func (h *CustomerHandler) OfficesList(gc *gin.Context) {
	id := gc.Query("customerId")
	offices, err := h.service.OfficesByCustomer(id)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: offices,
	})
}

func (h *CustomerHandler) OfficeServicesList(gc *gin.Context) {
	id := gc.Query("officeId")
	offices, err := h.service.ServicesByOffice(id)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: offices,
	})
}

func (h *CustomerHandler) ServicesList(gc *gin.Context) {
	id := gc.Query("customerId")
	services, err := h.service.ServicesByCustomer(id)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: services,
	})
}

func (h *CustomerHandler) Statistics(gc *gin.Context) {
	_, er := GenerateContext(gc.GetHeader("Authorization"))
	if er != nil {
		gc.JSON(http.StatusUnauthorized, gin.H{"error": errors.InvalidValidToken()})
		return
	}
	id := gc.Query("customerId")
	startDate := gc.Query("startDate")
	endDate := gc.Query("endDate")
	response, err := h.service.CustomerStats(id, startDate, endDate,
	)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		log.Println(err)
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: response,
	})
}

func (h *CustomerHandler) OfficeRating(gc *gin.Context) {
	officeID := gc.Query("officeId")
	startDate := gc.Query("startDate")
	endDate := gc.Query("endDate")
	ratings, err := h.service.OfficeRating(officeID, startDate, endDate)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data: ratings,
	})

}

func (h *CustomerHandler) Logos(gc *gin.Context) {
	res := h.service.Logos()

	gc.JSON(http.StatusOK, Response{
		Data: res,
	})
}
