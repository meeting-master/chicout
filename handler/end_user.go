package handler

import (
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/errors"
	model2 "gitlab.com/meeting-master/sdk/user/model"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (h *CustomerHandler) OfficesByIds(gc *gin.Context) {

}


func (h *CustomerHandler) OfficesByTag(gc *gin.Context) {
	tag := gc.Query("tag")
	townId := gc.Query("townId")
	lat := 0.0
	lon := 0.0
	latitude := gc.Query("lat")
	longitude:= gc.Query("lon")
	if lt, err := strconv.ParseFloat(latitude, 64); err == nil {
		lat = lt
	}

	if lo, err := strconv.ParseFloat(longitude, 64); err == nil {
		lon = lo
	}

	offices, err := h.service.OfficesByTag(tag, townId, lat, lon)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data:  offices,
	})
}

func (h *CalendarHandler) AppointmentHistory(gc *gin.Context) {
	id := gc.Query("id")
	offices, err := h.service.AppointmentHistory(id)
	if err != nil {
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	gc.JSON(http.StatusOK, Response{
		Data:  offices,
	})
}

func (h *CalendarHandler) InsertRating(gc *gin.Context) {
	var request model.RatingRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	res, err := h.service.InsertRating(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{ID: res})
}

func (h *CalendarHandler) DeviceToken(gc *gin.Context) {
	var request model.DeviceRequest
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	err := h.service.InsertDevice(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, "")
}

func (h *UserHandler) UpsertEndUser(gc *gin.Context) {
	var request model2.EndUser
	if err := gc.ShouldBindJSON(&request); err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": errors.InvalidRequestData()})
		return
	}

	err := h.service.UpsertEndUser(request)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, "")
}

func (h *UserHandler) GetEndUser(gc *gin.Context) {
	id := gc.Query("id")
	res, err := h.service.EndUser(id)
	if err != nil {
		log.Println(err.Error())
		gc.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	gc.JSON(http.StatusOK, Response{
		Data:  res,
	})
}