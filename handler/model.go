package handler

import "mime/multipart"

type Response struct {
	ID    string      `json:"ID"`
	Error string      `json:"error"`
	Data  interface{} `json:"response"`
}

type Request struct {
	ActivationCode string `json:"ActivationCode"`
	ID             string `json:"id"`
}

type CustomerForm struct {
	Data string                `form:"data"`
	Logo *multipart.FileHeader `form:"logo"`
}

type AnnounceForm struct {
	Data   string                  `form:"data"`
	Images []*multipart.FileHeader `form:"images"`
}
